---
layout: handbook-page-toc
title: "Data Team"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Data</strong>
  </p>
</div>
{:.no_toc}

----

## <i class="fab fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>Quick Links
[Primary Project](https://gitlab.com/gitlab-data/analytics/){:.btn .btn-purple}
[Issue Tracker](https://gitlab.com/groups/gitlab-data/-/issues){:.btn .btn-purple}
[Issue Intake](/handbook/business-ops/data-team/how-we-work/#data-analysis-process){:.btn .btn-purple}
[Sisense](https://app.periscopedata.com/app/gitlab/){:.btn .btn-purple}

| **SERVICES** | **SELF-SERVICE** | **TECH GUIDES** |  **INFRASTRUCTURE** |  **TEAM GUIDES** |
| --- | --- | --- | --- | --- |
| [KPI Index](/handbook/business-ops/data-team/kpi-index/#gitlab-kpis) | [SiSense](/handbook/business-ops/data-team/platform/periscope/) | [SQL Style Guide](/handbook/business-ops/data-team/platform/sql-style-guide/) | [Data Platform](/handbook/business-ops/data-team/platform/) | [How We Work](/handbook/business-ops/data-team/how-we-work/) |
| [KPI Development](/handbook/business-ops/data-team/kpi-index/#kpi-development) | [Snowflake EDW](/handbook/business-ops/data-team/platform/#warehouse-access) |  [dbt Guide](/handbook/business-ops/data-team/platform/dbt-guide/) | [CI Jobs](/handbook/business-ops/data-team/platform/ci-jobs/) | [Triage](/handbook/business-ops/data-team/how-we-work/duties/#data-triage) |
| [Data Analysis](/handbook/business-ops/data-team/how-we-work/#data-analysis-process) | [Enterprise Dimensional Model](/handbook/business-ops/data-team/platform/edw/)| [Python Style Guide](/handbook/business-ops/data-team/platform/python-style-guide/) | [Permifrost](/handbook/business-ops/data-team/platform/permifrost/) | [Calendar](/handbook/business-ops/data-team/how-we-work/calendar/) |
| [SheetLoad](/handbook/business-ops/data-team/platform/#using-sheetload) | [Data Sources](/handbook/business-ops/data-team/platform/#extract-and-load) | [Airflow & Kubernetes](/handbook/business-ops/data-team/platform/infrastructure/#common-airflow-and-kubernetes-tasks)  | [Snowplow](/handbook/business-ops/data-team/platform/snowplow/) | [Learning Library](/handbook/business-ops/data-team/learning-library/) |
| [Special Event On Call](/handbook/business-ops/data-team/data-service/#special-event-on-call) | [Data for Product Managers](/handbook/business-ops/data-team/programs/data-for-product-managers/) | [Docker](/handbook/business-ops/data-team/platform/infrastructure/#docker) | [Data Infrastructure](/handbook/business-ops/data-team/platform/infrastructure/) |

----

## <i class="far fa-compass fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>Mission

#### Deliver Results That Matter With Trusted and Scalable Data Solutions
{:.no_toc}
The Data Team strives to deliver high quality results that make a strategic impact with data solutions that can grow quickly and easily.

- Our [Direction](/handbook/business-ops/data-team/direction) discusses what we are doing over the coming months and quarters to support our mission.
- Our [Principles](/handbook/business-ops/data-team/principles) inform how we accomplish our mission.

## <i class="fas fa-tasks fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>Responsibilities

The Data Team is a Sub-department of the [Business Operations](/handbook/business-ops) Department and provides a Data & Analytics platform, programs, and services to the entire company. 

* Define and publish a Data Strategy to help maximize the value of GitLab's Data Assets
* Build and maintain the company's central Enterprise Data Warehouse to support Reporting, Analysis, [Dimensional Modeling](https://www.kimballgroup.com/data-warehouse-business-intelligence-resources/kimball-techniques/dimensional-modeling-techniques/), and Data Development for all GitLab teams
* Integrate new data sources to enable analysis of subject areas, activities, and processes
* Manage and govern the company's [Key Performance Indicator](/handbook/ceo/kpis) definitions, database, and data visualizations
* Build and maintain an Enterprise Dimensional Model to enable [Single Source of Truth](https://en.wikipedia.org/wiki/Single_source_of_truth) results
* Develop Data Management features such as master data, reference data, data quality, data catalog, and data publishing
* Support the company's governance, risk, and compliance programs as they relate to Data & Analytics systems
* Provide Self-Service Data capabilities to help everyone leverage data and analytics
* Help to define and champion Data Quality practices and programs for GitLab data systems
* Provide customizable Data Services, including Data Visualization, Data Modeling, Data Quality, and Data Integration
* Broadcast regular updates about data deliverables, ongoing initiatives, and upcoming plans

---

## <i class="fas fa-map-marked-alt fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>Navigating the Data Team Handbook

- [KPI Index](/handbook/business-ops/data-team/kpi-index)
- [How We Work](/handbook/business-ops/data-team/how-we-work)
	- [Calendar](/handbook/business-ops/data-team/how-we-work/calendar)
	- [Team Duties](/handbook/business-ops/data-team/how-we-work/duties)
- [Data Team Organization](/handbook/business-ops/data-team/organization)
	- [Data Engineering Team](/handbook/business-ops/data-team/organization/engineering)
- [Data Platform](/handbook/business-ops/data-team/platform)
	- [Sisense (Periscope)](/handbook/business-ops/data-team/platform/periscope)
	- [dbt Guide](/handbook/business-ops/data-team/platform/dbt-guide)
	- [Enterprise Data Warehouse](/handbook/business-ops/data-team/platform/edw)
	- [Data Infrastructure](/handbook/business-ops/data-team/platform/infrastructure)
	- [SQL Style Guide](/handbook/business-ops/data-team/platform/sql-style-guide)
	- [Python Style Guide](/handbook/business-ops/data-team/platform/python-style-guide)
	- [Permifrost](/handbook/business-ops/data-team/platform/permifrost)
	- [Snowplow](/handbook/business-ops/data-team/platform/snowplow)
	- [Data CI Jobs](/handbook/business-ops/data-team/platform/ci-jobs)
- Data Programs
	- [Data for Finance](/handbook/business-ops/data-team/programs/data-for-finance)
	- [Data for Product Managers](/handbook/business-ops/data-team/programs/data-for-product-managers)
	- [Data Champion](/handbook/business-ops/data-team/programs/data-champion)
- [Data Quality](/handbook/business-ops/data-team/data-quality)
    - [Trusted Data Framework](/handbook/business-ops/data-team/direction/trusted-data)
- [Data Quality Process](/handbook/business-ops/data-team/data-quality-process)
- [Data Team Principles](/handbook/business-ops/data-team/principles)
- [Data Service](/handbook/business-ops/data-team/data-service)
- [Data Handbook Documentation](/handbook/business-ops/data-team/documentation)
- [Data Learning and Resources](/handbook/business-ops/data-team/learning-library)

---

## <i class="fas fa-bullhorn fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>Contact Us

* [Data Team Project](https://gitlab.com/gitlab-data/analytics/)
* [#data](https://gitlab.slack.com/messages/data/) on Slack
* [What time is it for folks on the data team?](https://www.worldtimebuddy.com/?pl=1&lid=2950159,6173331,4487042,4644585&h=2950159)

#### <i class="fab fa-slack fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>Slack 

The Data Team primarily uses these channels on Slack:

* [#data](https://gitlab.slack.com/messages/data/) is the primary channel for all of GitLab's data and analysis conversations. This is where folks from other teams can link to their issues, ask for help, direction, and get general feedback from members of the Data Team.
* [#data-daily](https://gitlab.slack.com/messages/data-daily/) is where the Data Team tracks day-to-day productivity, blockers, and fun. Powered by [Geekbot](https://geekbot.com/), it's our asynchronous version of a daily stand-up, and helps keep everyone on the Data Team aligned and informed.
* [#data-lounge](https://gitlab.slack.com/messages/data-lounge/) is for links to interesting articles, podcasts, blog posts, etc. A good space for casual data conversations that don't necessarily relate to GitLab. Also used for intrateam discussion for the Data Team.
* [#data-engineering](https://gitlab.slack.com/messages/data-engineering/) is where the GitLab Data Engineering team collaborates.
* [#business-operations](https://gitlab.slack.com/messages/business-operations/) is where the Data Team coordinates with Business Operations in order to support scaling, and where all Business Operations-related conversations occur.
* [#analytics-pipelines](https://gitlab.slack.com/messages/analytics-pipelines/) is where slack logs for the ELT pipelines are output and is for data engineers to maintain.  The DRI for tracking and triaging issues from this channel is shown [here](/handbook/business-ops/data-team/platform/infrastructure/#data-infrastructure-monitoring-schedule)
* [#dbt-runs](https://gitlab.slack.com/messages/dbt-runs/), like #analytics-pipelines, is where slack logs for [dbt](https://www.getdbt.com/) runs are output. The Data Team tracks these logs and triages accordingly.
* [#data-triage](https://gitlab.slack.com/messages/data-triage/) is an activity feed of opened and closed issues and MR in the data team project.

You can also tag subsets of the data using:

* @datateam - this notifies the entire Data Team
* @data-engineers - this notifies just the Data Engineers
* @data-analysts - this notifies just the Data Analysts

Additional channels which are not guaranteed to be long-lived are:

* [#data-fy21-q3-platform-and-strategic-project](https://gitlab.slack.com/messages/data-fy21-q3-platform-and-strategic-project/) - For the Platform Team for FY21-Q3
* [#data-fy21-q3-product-analytics-fusion-team](https://gitlab.slack.com/messages/data-fy21-q3-product-analytics-fusion-team/) - For the Product Analytics Fusion Team for FY21-Q3
* [#data-fy21-q3-lead-to-cash-people-and-operations-project](https://gitlab.slack.com/messages/data-fy21-q3-lead-to-cash-people-and-operations-project/) - For the Lead to Cash, People and Operations Fusion Team for FY21-Q3
