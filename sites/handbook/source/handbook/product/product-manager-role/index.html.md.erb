---
layout: handbook-page-toc
title: The Product Manager Role at GitLab
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles) - [**Processes**](/handbook/product/product-processes) - [**Categorization**](/handbook/product/product-categories) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

## Product Leadership

The separate page [Product Leadership](/handbook/product/product-leadership/) covers how to be an effective leader in the product management organization.

## Product Organizational Structure

The GitLab Product team includes team members at various levels of [Product Management job titles](/handbook/product/product-manager-role/#product-management-career-development-framework) across our [organizational levels](/company/team/structure/#levels) with scope at various points in our [product hierarchy](/handbook/product/product-categories/#hierarchy). As a result there can be instances where peers across layers don't have the same title. We will always abide by [GitLab's layer structure](/company/team/structure/#layers).

| Level | Job Families | Hierarchy Scopes |
| ----- | ------------ | ---------------- |
| IC | [Product Manager](/job-families/product/product-manager/) | Group, Stage |
| Manager | [Group Manager Product](/job-families/product/group-manager-product/), [Director of Product](/job-families/product/director-of-product/) | Collection of Groups, Stage, Section |
| Director | [Director of Product](/job-families/product/director-of-product/) | Section |
| Senior Leader | [VP](/job-families/product/vp-of-product/) | All Sections |
| Executive | [EVP](/job-families/product/evp-of-product/) | Entire Function |

## Product Management Job Descriptions

- [Product Manager](/job-families/product/product-manager/)
- [Group Manager of Product](/job-families/product/group-manager-product/)
- [Director of Product](/job-families/product/director-of-product/)
- [VP of Product](/job-families/product/vp-of-product/)

## How to work as a PM

If you follow the [Product Principles](/handbook/product/product-principles) and [Product Processes](/handbook/product/product-processes) in our handbook, you won't be writing long, detailed
specs for a part of the product for next year. So how should you be spending your time?

Invest the majority of your time (say 70%) in deeply understanding the problem.
Then spend 10% of your time writing the spec _for the first iteration only_ and
handling comments, and use the remaining 20% to work on promoting it.

A problem you understand well should always have a (seemingly) simple or obvious
solution. Reduce it to its simplest form and only ship that as an [MVC](/handbook/product/product-principles/#the-minimal-viable-change-mvc).

Once you've shipped your solution, both you and the community will
have a much better idea of what can be improved and what should be prioritized
for future iterations.

As a PM, you're the person that has to kick-off new initiatives. You're not
responsible for shipping something on time, but you _are_ responsible for taking
action and setting the direction. Be active everywhere, over-communicate, and
sell the things you think are important to the rest of the team and community.

As a PM, you need to set the bar for engineering. That is, to push engineering and
the rest of the company. You almost want engineering to complain about the pace
that product is setting. Our default instinct will be to slow down, but we can't
give in to that.

As a PM you don't own the product; ask other people for feedback and give
team members and the community the space to suggest and create things without your
direct intervention. It's your job to make sure things are decided and
planned, not come up with every idea or change.

### Where should you look when you need help?

- The first thing you should do is read this page carefully, as well as the
[general handbook](/handbook/).
- You can ask questions related to Product in the `#product` Slack channel.
- General questions should be asked in `#questions`.
- Specific Git related questions should be asked in `#git-help`.
- HR questions should be asked in `#peopleops`.

### Responsibilities and Expectations

The responsibilities for [Product Managers, Sr. Product Managers, and Principal Product Managers](/job-families/product/product-manager/), [Group Manager, Product Management](/job-families/product/group-manager-product/)
[Director of Product](/job-families/product/director-of-product/),
[VP of Product](/job-families/product/vp-of-product/) and
[VP of Product Strategy](/job-families/product/vp-of-product-strategy/) are outlined in our job families pages.

The progression of responsibilities allocation between tactical, operational and strategic
is well illustrated by this helpful chart.

![GitLab PM Responsibility Allocation Chart](/handbook/product/pm-allocation.png)

_[Source File](https://docs.google.com/spreadsheets/d/19gAgPJVdXfBpXiFOlT1WqZLJB_eFU7W7slmAzodEuDM/edit#gid=1012729771). Note - Thanks to [Melissa Perri](https://twitter.com/lissijean/) for the inspiration_

In addition, as a Product Manager you're expected to:

- Use the product and deeply understand the features you're responsible for.
- Follow the issues you've been involved with / are assigned to as a PM. That
includes reading all comments. Use email notifications for this.
- Make sure the issue description and title is updated when necessary. It
should always reflect the current state of the issue.
- Make sure issues are moved forward when needed. You should not only avoid
being the bottleneck, you should also be the person moving issues forward when
they get stuck or overlooked.
- Make sure features solve the original problem effectively.
- Make sure features are complete: documentation, marketing, API, etc.
- Know when to cut corners and when not to. If we merge documentation a day
later, that's usually acceptable. Conversely though, learning from a customer
that documentation is lacking is not.
- Excite and market new features and changes internally and externally.
- Help build a plan and strategy for GitLab and GitLab's features.
- Understand deeply whatever it is you're working on. You should be spending a
lot of time learning about your subject matter.
- Have regular meetings (at least once a week) with customers.
- Make sure marketing materials related to your work are up to date.
- Ensure that `data/categories.yml` is up to date, including accurate links based on the defined link [hierarchy](/handbook/product/product-categories/).

Occasionally, Product Managers are asked to perform the role of "Life Support" Product Manager for
a group. When doing so please refer to the [Life Support PM expectations](#life-support-pm-expectations).

As we grow, Product Managers can be listed across multiple stages and be asked to
perform the role of Product Manager across split or multiple teams in an "Interim" basis. While
temporary and based on future hiring, these positions are not considered
"Life Support" and thus the standard Responsibilities and Expectations apply.

### Scope of responsibilities

The product team is responsible for iteration on most of GitLab's products and
projects:

- GitLab CE and EE
- GitLab.com
- about.gitlab.com
- customers.gitlab.com
- version.gitlab.com
- license.gitlab.com

This includes the entire stack and all its facets. The product team needs to
weigh and prioritize not only bugs, features, regressions, performance, but also
architectural changes and other changes required for ensuring GitLab's excellence

### Direction

Our product wide, section, stage and category direction pages can be found from our [Direction](/direction) page.

As this document and the [direction page](/direction) shows,
there are a million things we want to do.
So, how do we prioritize them and schedule things properly?
A [product group](/company/team/structure/#product-groups)'s [PM is the DRI for prioritization](#pm-em-ux-and-qe-quad-dris) and per our DRI definition they do not [owe an explanation for their decisions](/handbook/people-group/directly-responsible-individuals/#empowering-dris-no-explanation-needed) because we don't want PMs to fall into a perpetual loop of explaining and prefer a [bias for action](/handbook/values/#bias-for-action). However, [one responsibility of a PM](/job-families/product/product-manager/#responsibilities) is to build rapport and align your stakeholders on priorities. It's almost always in a PMs best interest to explain why they are making prioritization calls.

### Important dates PMs should keep in mind

**4th of the Month:**

Draft of the issues that will be included in the next released (released 22nd of next month).
Start capacity and technical discussions with engineering/UX.

**12th of the Month:**

Release scope is finalized. In-scope issues marked with milestone
Kickoff document is updated with relevant items to be included.

**15th of the Month:**

Group Kickoffs calls recorded and uploaded by the end of the day.

Also see [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline).

## Product Management Career Development Framework

<kbd class="required">Required 🔎</kbd><br>
<br>
We track progress through the skills required to be a product manager at all levels via the Product Management Career Development Framework (CDF). The single source of truth for that framework is the table below but you can use this [GoogleSheet template](https://docs.google.com/spreadsheets/d/1NaqSgu_1IcL_DYRHjrDGsKLooSykpewD3QYU2OZR5jc/edit#gid=1091464991) to track your career development with your manager.

| IC Title | PM | Sr. PM | Principal PM | |

| Manager Title |  |  | Group Manager PM | Director PM |
| ------------- | --- | --- | ---------------- | ----------- |
| **Validation Track Skills** | Adept at qualitative customer interviewing. Familiar with prioritization frameworks like RICE to organize opportunity backlogs. Capable of deriving key insights and patterns from customer interviews, and using that input to clarify problem statements. Proficient at story mapping, to break epics down into smaller [Minimal Viable Change (MVC)](/handbook/values/#minimal-viable-change-mvc) issues. Proficient at collaborating with Design on protoypes to bring potential solutions to life. | Skilled at qualitative customer interviewing. Excellent at deriving key insights and patterns from customer interviews, and using that input to clarify problem statements. Skilled at story mapping, to break epics down into smaller MVC issues. Excellent at collaborating with Design on protoypes to bring potential solutions to life. | Skilled at qualitative customer interviewing. Skilled at story mapping, to break epics down into smaller MVC issues. Capable of teaching product validation techniques to others. | Ensures consistent execution of validation track skills across a large team. |
| **Build Track Skills** | Adept at breaking epics and issues down into MVC's. Knowledgeable about GitLab's product and the relevant product domain(s). Knowledgeable about GitLab's architecture, API's, and tech stack. Capable of running a demo anytime. Able to make highly informed prioritization & tradeoff decisions with engineering. Able to discuss & evaluate technical architecture recommendations from eng. Responsible for the health of working relationships with peers in the Group. Familiar and comfortable with agile development methodologies. | Excellent at breaking epics and issues down into MVC's. Deep familiarity with GitLab's product and the relevant product domain(s). Deep understanding of GitLab's architecture and tech stack. Able to elevate performance of the Group through excellent PM outputs. | Expert at breaking epics and issues down into MVC's. Expert in the relevant product domain(s) and capable of teaching others about the domain. Responsible for the health of working relationships with fellow Engineering Managers. | Ensures consistent execution of build track skills across a large team. Responsible for the health of working relationships with fellow Engineering Directors. |
| **Business Skills** | Understands and communicates the business value of epics and issues. Sets success metrics for epics and issues, and tracks metrics post-launch to guide investment in iterative improvements. Spends up to 20% of time researching & defining category vision & strategy. | Able to ensure activities are consistent with GitLab's go-to-market and business model. Can balance build, buy and partner options for solving customer problems. Can identify new market opportunities & author business cases, as well as forecast the approximate benefits of new features. Spends up to 30% of time researching & defining category vision & strategy. | Expert at business case creation. Capable of managing business results across a range of product domains. | Works cross-stage and cross-functionally to ensure an excellent end-to-end customer experience. Excellent at understanding and managing the business impact across a wide range of product domains. Capable of making key pricing & packaging recommendations. |
| **Communication Skills** | Capable written and verbal communicator internally and externally. Drives clarity in area. Trusted resource for customer calls and meetings. Builds rapport with stakeholders to align around priorities. Self aware and understands how their interactions impact others. Takes action to improve behavior based on impact to others. | Capable of representing GitLab externally at trade shows, customer events, conferences, etc. Solid presentation skills at all levels of the company. Appropriately influences & persuade others to a course of action. | Recognized as a thought leader internally and externally. Excellent presentation skills at all levels of the company. Escalates issues cleanly to appropriate levels of authority when decisions or progress are blocked. | Visible leader across teams. Establishes compelling team purpose that is aligned to the overall organizational vision. Inspires broader team to achieve results. Identifies disconnects to vision and takes appropriate action. |
| **People Management Skills** | N/A | N/A | Aligns team with larger Stage vision and goals. Translates and aligns strategy in a meaningful way for team, building a shared understanding of team goals and targets. Uses situational leadership techniques to provide appropriate level of guidance and latitude to team members. Proactively identifies and fills talent gaps. Adept at caring personally for team members and providing candid real-time feedback. | Aligns team with larger Section vision and goals. Provides appropriate level of guidance and latitude to managers and individuals. Experienced at hiring and at managing out underperformance. Excellent at caring personally for team members and providing candid real-time feedback. |
| **Typical Reporting Structure** | Reports to a Director or Group Manager | Reports to a Director or Group Manager | Reports to a Director or VP | Reports to VP |
| **Typical Experience** | Two to four years of relevant experience or equivalent combination of experience and education. | Four to six years of relevant experience or equivalent combination of experience and education. | Eight to ten years of experience with at least one year of people management responsibility. | Ten to twelve years of experience with at least four years of people management responsibility. |

### CDF Reviews

Product Management Leadership will perform CDF reviews with their team members on a regular cadence; the minimum frequency is every 2 months. Consider creating a recurring meeting dedicated to this topic rather than utilizing existing 1:1 meetings.

After CDF reviews, if warranted, the Product Management Leader will discuss updated [compa group](/handbook/total-rewards/compensation/compensation-calculator/#compa-group) ratings with their Manager as part of their next 1:1. This ensures all leaders in the management chain are aware of progress and makes [compensation](/handbook/total-rewards/compensation/compensation-review-cycle/) and other [performance assessment](/handbook/people-group/performance-assessments-and-succession-planning/#talent-assessment-options) updates less surprising and more iterative.

## Competencies

Product Managers must be able to demonstrate a variety of skills across our CDF. We provide additional career development guidance by listing priority [competencies](/handbook/competencies/) for each CDF category.

At the moment we are working building out our Product Management Competencies.

### Validation Track Competencies

The [validation track](/handbook/product-development-flow/#validation-track) is the first phase of GitLab's Product Development Workflow. Industry wide, this track is commonly referred to as ideation and research. This phase lays the foundation for designs and solutions. When the business and users problems are well understood, the more likely it is that the product will solve the user's problem and the higher likelihood that those users will enjoy the solution.

Using a competencies model will help Product Managers understand the expectations of the validation track at each level in the Product Management organization. The competencies in the validation track are:

1. Customer Interviewing
1. Creating an Opportunity Canvas
1. Creating a Storyboard Designing Prototypes
1. Engaging Analysts via Inquiries

#### Customer Interviewing Competency

Customer interviewing is essential to clearly defining the user's needs, problems, and jobs to be done. At GitLab, customer interviewing is typically done in partnership with User Experience, including Product Design and User Research. This competency will help Product Managers get stronger at problem identification and curating a solid foundation for user stories, prototypes and solutions to be built upon.

| **Level** | **Demonstrates Competency by...** | **Assessment** |
| ----- | ----------------------------- | ---------- |
| **PM** | Adept at qualitative customer interviewing. Uses templates and already available resources for discussion guides. 50% or greater reliance on UX research for interviewing. Capable of deriving key insights and patterns from customer interviews, and using that input to clarify problem statements. Potentially misses cross-stage or outside primary persona or use cases. Capable of completing the guidelines referenced in the [Validation Track](/handbook/product-development-flow/#validation-track) handbook page. Low to moderate confidence in conducting and moderating [user interviews](/handbook/engineering/ux/ux-research/#for-user-interviews). Aware of [RICE](https://www.productplan.com/glossary/rice-scoring-model/) as a priority setting tool and can apply the framework assisted. | [Customer Interview Assessment - Individual Contributors](https://forms.gle/zMyvXPE8EeSvjbVg9) |
| **Sr. PM** | Skilled at qualitative customer interviewing. Actively improves existing resources and templates. 30% or less reliance on UX research for interviewing. Excellent at deriving key insights and patterns from customer interviews, and using that input to clarify problem statements. Independently identifies and brings in cross-stage representation during interviews with non-primary personas or use cases.<br>Skilled at applying and executing against the [Validation Track](/handbook/product-development-flow/#validation-track) in the handbook. Moderate to high confidence in conducting or moderating [user interviews](/handbook/engineering/ux/ux-research/#for-user-interviews) independently. Capable of applying [RICE](https://www.productplan.com/glossary/rice-scoring-model/) as a priority setting tool unassisted. | [Customer Interview Assessment - Individual Contributors](https://forms.gle/zMyvXPE8EeSvjbVg9) |
| **Principal PM/ Group Manager PM** | Highly skilled at qualitative customer interviewing. Coaches and continuously seeks feedback for existing resources on interviewing. Minimal reliance on UX research for interviewing and leverages UX research for consultation of research strategy. Contributes to goal setting and OKR development across the team. Seeks opportunities for cross-stage collaboration and validation from ancillary use cases or personas. Iterates and engages with the [Validation Track](/handbook/product-development-flow/#validation-track) in the handbook as new learnings arise. Typically conducts or moderates [user interviews](/handbook/engineering/ux/ux-research/#for-user-interviews) independently. Capable of applying [RICE](https://www.productplan.com/glossary/rice-scoring-model/) as a priority setting tool unassisted. | [Customer Interview Assessment - Individual Contributors](https://forms.gle/zMyvXPE8EeSvjbVg9)<br>Customer Interview Assessment - People Leaders - Coming Soon |
| **Director PM** | Ensures consistent execution of validation track skills across product groups. Seeks feedback and continuous refinement of validation processes. Measures and evaluates validation track performance on SMAU to ensure the process is delivering results for the business. | Customer Interview Assessment - People Leaders - Coming Soon |
| **Senior Director PM** | In addition to upholding director requirements, senior directors work to ensure the validation track appropriately includes external teams like UX, UX research, Design, or Engineering, as necessary. Developing awareness and driving collaboration with the track within R&D. | Customer Interview Assessment - People Leaders - Coming Soon |
| **Vice President PM** | In addition to the requirements of the senior director requirements, vice president’s proactively inform the validation process, goals, and frameworks with context from the global company and external stakeholders, like investors. | Customer Interview Assessment - People Leaders - Coming Soon |
| **EVP/Chief Product** | In addition to upholding the requirements of a VP, the executive vice president should advocate for the validation framework and goals across the company. Comfortable representing the way product decisions are informed and designed within and outside of the company. | Customer Interview Assessment - People Leaders - Coming Soon |

### Business Competencies
Product Managers are responsible for driving the success for a product area. Understanding market dynamics along side the ability to organize, influence, and iterate to reach the desired outcomes is essential. 

#### Understanding Buyer Based Tiering
GitLab has an [Open Core](https://en.wikipedia.org/wiki/Open-core_model) business model and generate the majority of revenue from [paid tiers](/pricing/). While general pricing decisions are made by the CEO, [determining pricing tier for product features](/handbook/ceo/pricing/#departments) is an important aspect of the Product Manager's job. We use a [buyer based tiering](/handbook/ceo/pricing/#buyer-based-tiering-clarification) model to determine which of the [four tiers](/handbook/ceo/pricing/#four-tiers) a feature belongs to. Being able to clearly define and articulate pricing decisions and strategy is a critical competency for PMs to master in order to influence the success of GitLab's business.

|  **Level** | **Demonstrates Competency by...**| **Assessment** |
| --- | --- | --- |
|**PM**| Are the pricing expert for their respective groups. Comprehends impact of features on pricing and capable of making informed decisions on feature alignment tier with minimal assistance. Understands the GitLab business model and can explain why GitLab is [sold as a suite](/handbook/ceo/pricing/#only-sell-a-suite). Leads group to fulfill GitLab's [stewardship promise](/company/stewardship/#promises). Adept at applying [buyer based tiering](/handbook/ceo/pricing/#buyer-based-open-core) when determining pricing tiers. When necessary, prioritizes [quickly moving features down tiers](/handbook/ceo/pricing/#if-a-feature-can-be-moved-down-do-it-quickly). Active in learning the buyer personas and common go-to-market motions for relevant categories and stages so that they can prioritize the right features in line with GitLab's [pricing philosophy](https://about.gitlab.com/handbook/ceo/pricing/#pricing-philosophy). | TBD |
|**Sr. PM**| Skilled at applying buyer based tiering when determining the pricing tier for a feature. Thinks holistically and leverages [capabilities](/handbook/ceo/pricing/#capabilities) to drive adoption for relevant features in paying tiers. Builds logical ramps to enable customers to move up in tier by prioritizing features valued by [advocates and champions](/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/#312-finding-your-champions). Provides guidance and coaching to team members and fellow Product Managers to help drive consistency in tiering strategy. | TBD |
|**Principal PM/ Group Manager PM**| Highly skilled at applying buyer based tiering when determining the pricing tier for a feature. Deeply familiar with the buyer persona and go-to-market motion in relevant stages or categories and can identify and lead their group to find the right balance between [revenue, cost, and value](/handbook/ceo/pricing/#when-is-a-dollar-not-a-dollar) for GitLab and its customers. Supports the [hybrid sales model](/handbook/ceo/pricing/#hybrid-sales-model) by simultaneously building a pipeline of self-service GitLab users with useful features in core and starter and provide higher tier value by building indispensable enterprise features and cross-stage integrations. | TBD |

|**Director PM**| Ensures consistent application of buyer based tiering across product stages. Coaches Product Managers on market-specific buyer needs and go-to-market strategy. Applies [consumption pricing principles](/handbook/ceo/pricing/#consumption-pricing-principles) to help simplify pricing and make GitLab more scalable. Ensures [tiering and pricing changes](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=Feature-Tier-Or-Pricing-Change) do not have any preventable negative financial or business model impact. | TBD |


### Communication Competencies

[Product Managers communicate](/handbook/product/product-processes/#communication) to various audiences across an array of medium. As a result the set of communication competencies for Product Managers is more diverse than those for other positions.

#### Writing to Inspire Action Competency

[Writing to inspire action](/handbook/product/product-processes/#writing-to-inspire-action) is a critical competency for PMs in order to facilitate a bias towards action within their [Product Groups](/company/team/structure/#product-groups). PMs communicate with a wide variety of audiences one key to inspiring action via your writing is focusing on what your reader values.

### Build Track Competencies
The [build track](/handbook/product-development-flow/#build-track) is the second phase of GitLab's Product Development Workflow. This track is the execution phase for the solutions that were validated and designed during the Validation track.

Using a competencies model will help Product Managers understand the expectations of the build track at each level in the Product Management organization. The competencies in the build track are:

1. Breaking Down Your Issues
1. Optimizing PM Inputs for Development Outputs
1. Understanding GitLab Architecture
1. Demoing GitLab

#### Breaking Down Issues
The ability to break down larger features and functionality into small iterative issues is a key skill of any Product Manager. Breaking down issues into small iterations enables the development team to quickly understand what is the next most important iteration to execute.  To be skilled in this competency, a Product Manager must have a deep understanding of the customer problem to be solved, a strong familiarity with how GitLab is architected, and familiar with how software applications function.

|  **Level** | **Demonstrates Competency by...**| **Assessment** |
| --- | --- | --- |
| **PM** | Adept at breaking solutions down into issues for development. Learning the [iteration strategies](/handbook/product-development-flow/#iteration-strategies) outlined in the [Product Development Framework](/handbook/product-development-flow/) and implementing them in collaboration with their Engineering Manager. Active in their pursuit to understand underlying GitLab's [technical architecture](https://docs.gitlab.com/ee/development/architecture.html) to aid in their ability to make iterations as small as possible. Seeks feedback from their Engineering Manager, their team, and other Product Managers on issues during the planning process to make sure that they are considering the different ways that issues can be broken down and working to [lower their level of shame](/handbook/values/#low-level-of-shame)|TBD|
| **Sr. PM** |Skilled at breaking solutions down into issues for development. Regularly practices all [iteration strategies](/handbook/product-development-flow/#iteration-strategies) outlined in the [Product Development Framework](/handbook/product-development-flow/) without direct support from their engineering manager. Proactively requests reviews from Engineering to make sure that issues have been broken down and refined early in the [planning cycle](/handbook/product-development-flow/#build-phase-1-plan). Familiar with GitLab’s underlying [technical architecture](https://docs.gitlab.com/ee/development/architecture.html) and is able to leverage that knowledge when breaking down issues. |TBD|
| **Principal PM/ Group Manager PM** |Highly skilled at breaking solutions down into issues for development. All proposed Issues are [MVCs](/handbook/values/#minimal-viable-change-mvc) and logically broken down based on the [iteration strategies](/handbook/product-development-flow/#iteration-strategies) outlined in the [Product Development Framework](/handbook/product-development-flow/). Seeks ways in which to empower their team members to break down issues to extend themselves. Provides guidance and coaching to team members and fellow Product Managers in the ways that issues can be smaller.|TBD|
| **Director PM** |  Coaches Product Managers on the application of iteration strategies and how to achieve lower levels of shame. Ensures consistent issue breakdown across product groups. Seeks feedback to continuously refine the break-down process. Collaborates with Engineering Directors in their sections/stages to ensure [Development PIs](/handbook/engineering/development/performance-indicators/) are achieved via iteration.|TBD|
| **Senior Director PM** |TBD||
| **Vice President PM** |TBD||
| **EVP/Cheif Product** |TBD||


## Future Competencies

Here is our prioritized list of future competencies. We'll add them to our competencies list by starting with the top priority in each skill category. We'll add more detail and content for each one of these as we add them to the PM competencies list.

Validation Track Skills

1. Creating an Opportunity Canvas
1. Creating a Storyboard Designing Prototypes
1. Engaging Analysts via Inquiries

Build Track Skills
1. Optimizing PM Inputs for Development Outputs
1. Understanding GitLab Architecture
1. Demoing GitLab

Business Skills

1. Crafting a Strong Vision
1. Defining & Prioritizing for Success Metrics
1. Developing a Business Case
1. Investing Just Enough

Communication Skills

1. Documenting for Clarity
1. Communicating to Executives
1. Presenting to Large Audiences
1. Representing GitLab's Entire Product Value

People Management Skills

1. Managing Team Performance
1. Facilitating Career Development Conversations
1. Coaching GitLab Values Based Product Management
1. Aligning Your Team on Strategy
1. Applying Situational Leadership

## Contributing to our PM Competencies

Every GitLab team member is encouraged to contribute to the list of prioritized Product Manager competencies, as well as the content for each competency via merge requests to this page.

### Product Manager Onboarding

Product Manager onboarding, beyond any [product specific steps in your first week onboarding ticket](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding_tasks/department_product_management.md),
is defined in the Product projects [PM Onboarding issue template](https://gitlab.com/gitlab-com/Product/blob/master/.gitlab/issue_templates/PM-onboarding.md).

Onboarding issues can be tracked in the [Product Onboarding Issue Board](https://gitlab.com/gitlab-com/Product/-/boards/1283554?&label_name%5B%5D=onboarding).

Iteration on Product Management Onboarding is encouraged by all team members. To do so, create an MR against one of the above files and assign it to your manager for review and merge.

## Interviewing Product Management Candidates

A unique and important step in the interview process for Product Management candidates is our Deep Dive Interview. The goal of this interview is to understand the candidate's ability to communicate a long term vision as well as a short term MVC, both verbally during the interview itself, and written via two follow up issues. Once the issues are ready for you to read, it is an opportunity to provide feedback and see how the candidate responds to that feedback.

You can find more information and instructions on the Deep Dive interview [here](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Product/DeepDive.md). For information on our hiring process, head over to our [hiring handbook pages](/handbook/hiring/). |
