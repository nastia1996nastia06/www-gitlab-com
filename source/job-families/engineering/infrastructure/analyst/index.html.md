---
layout: job_family_page
title: "Infrastructure Analyst"
---

# Infrastructure Analyst Roles at GitLab
The **Infrastructure Department** is the primary responsible party for the **availability**, **reliability**, **performance**, and **scalability** of all user-facing services (most notably **GitLab.com**). Other departments and teams contribute greatly to these attributes of our service as well. In these cases it is the responsibility of the Infrastructure Department to close the feedback loop with monitoring and metrics to drive accountability, and these include KPIs and financial data.

The Infrastructure Analyst is a key member of the Infrastructure team and works to enhance and improve our business operations and our forecasting and financial modeling capabilities, developing sound business data management practices within the Infrastructure Department to guide infrastructure resource utilization and associated costs optimizations over time, as well as tracking and modeling of all relevant KPIs.

This role will require an analytical and business-oriented mindset with the ability to implement rigorous database solutions and best practices in order to produce and influence the adoption of strong quality data insights to drive business decisions within the Infrastructure Department.


## Responsibilities

* Develop infrastructure resource utilization and forecasting models to meet business and financial objectives
* Develop infrastructure financial models to provide data-driven guidance on cost decisions
* Interface with Finance, Legal and vendors to manage Infrastructure-related contracts
* Interface with Sales to understand sales pipeline and its effect on Infrastructure
* Develop and maintain Infrastructure-centric sales collateral as it relates to GitLab.com
* Manage service level framework (SLIs, SLOs, SLAs) and associated error budgets to meet business objectives for GitLab.com
* Manage Infrastructure's [Performance Indicators](/handbook/engineering/infrastructure/performance-indicators/)
* Review Infrastructure business processes and policies and help enhance workflows in support of GitLab.com and Infrastructure-provided services to the rest of the company
* Develop a deep understanding of the infrastructure vendor landscape to help Infrastructure leaders select, work and optimize infrastructure usage
* Explain trends across data sources, potential opportunities for growth or improvement, and data caveats for descriptive, diagnostic, predictive (including forecasting), and prescriptive data analysis
* Deep understanding of how data is created and transformed through Infrastructure to help drive product designs or service usage or note impacts to data reporting capabilities
* Understand and document the full lifecycle of Infrastructure data and our common data framework so that all data can be integrated, modeled for easy analysis, and analyzed for data insights
* Document every action in either issue/MR templates, the [handbook](/handbook/), or READMEs so your learnings turn into repeatable actions and then into automation following the GitLab tradition of [handbook first!](/handbook/handbook-usage/#why-handbook-first)
* Expand our database with clean data (ready for analysis) by implementing data quality tests while continuously reviewing, optimizing, and refactoring existing data models
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale database environment. Maintain and advocate for these standards through code review
* Follow and improve our processes and workflows for maintaining high quality data and reporting while implementing the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* This position reports to the Senior Director of Infrastructure


## Requirements

* 2+ years experience in an analytics role
* Experience building reports and dashboards in a data visualization tool
* Passionate about data, analytics and automation. Experience cleaning and modeling large quantities of raw, disorganized data (we use dbt)
* Experience with a variety of data sources.
* Demonstrate capacity to clearly and concisely communicate complex business logic, technical requirements, and design recommendations through iterative solutions
* Deep understanding of SQL in analytical data warehouses (we use Snowflake SQL) and in business intelligence tools (we use Periscope)
* Hands on experience working with SQL, Python, API calls, and JSON, to generate business insights and drive better organizational decision making
* Familiarity with Git and the command line
* Deep understanding of relational and non-relational databases, SQL and query optimization techniques, and demonstrated ability to both diagnose and prevent performance problems
* Effective communication and [collaboration](/handbook/values/#collaboration) skills, including clear status updates
* Positive and solution-oriented mindset
* Comfort working in a highly agile, [intensely iterative](/handbook/values/#iteration) environment
* [Self-motivated and self-managing](/handbook/values/#efficiency), with strong organizational skills
* Ability to thrive in a fully remote organization
* Share and work in accordance with our values
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
* Ability to use GitLab


## Levels

Read more about [levels at GitLab](/handbook/hiring/#definitions).

### Infrastructure Analyst
The Intermediate Infrastructure Analyst role has the requirements and responsibility as outlined above.

#### Job Grade

The Infrastructure Analyst is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Senior Infrastructure Analyst
All of the responsibilities of a Intermediate Infrastructure Analyst, plus:
* Advocate for improvements to data quality, security, and query performance that have particular impact across your team as a Subject Matter Expert (SME)
* Solve technical problems of high scope and complexity
* Exert influence on the overall objectives and long-range goals of your team
* Understand the code base extremely well in order to lead new data innovation and to spot inconsistencies and edge cases
* Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems
* Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment; Maintain and advocate for these standards through code review
* Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions
* Provide mentorship for Junior and Intermediate Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy
* Confidently deliver and explain data analytics methodologies and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects
* Build close relationships with other functional teams to truly democratize data understanding and access
* Influence and implement our service level framework [SLOs](/handbook/business-ops/data-team/platform/#slos-service-level-objectives-by-data-source) and SLAs for our data sources and data services
* Identifies changes for the product architecture and from third-party services from the reliability, performance and availability perspective with a data driven approach focused on relational databases, knowledge of another data storages is a plus
* Proactively work on the efficiency and capacity planning to set clear requirements and reduce the system resources usage to make compute queries cheaper

#### Job Grade

The Senior Infrastructure Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

***

### Staff Infrastructure Analyst
The Staff Infrastructure Analyst role extends the [Senior Infrastructure Analyst](#senior-infrastructure-analyst) role.

#### Job Grade

The Infrastructure Analyst is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Drive innovation across GitLab with a willingness to experiment and to boldly confront problems of immense complexity and scope
* Actively seeks out difficult impediments to our efficiency as a team ("technical debt"), propose and implement solutions that will enable the entire team to iterate faster
* Exert significant influence on the overall objectives and long-range goals of GitLab, and in particular, of the Infrastructure Department.
* Reviews and analyzes query performances to add new best practices to our data analysis coding standards
* Ensure that our standards for style, maintainability, and best practices are suitable for the unique problems of scale and diversity of use. Maintain and advocate for these standards through code review
* Confidently researches new data analytics methodologies with minimal guidance and support from other team members. Collaborate with the team on larger projects
* Act as an expert in the Data Analytics industry and in GitLab by setting the strategic direction and the short term goals of the Data Team, focusing on team improvements
* Provide mentorship for all team members to help them grow in their technical responsibilities as they relate to data analytics, removing blockers to their autonomy.
* Create training guides, blog posts, templates, and recorded training sessions to help Infrastructure Department staff understand how to accurately view data, use data for insights, and the implications of data-driven analysis in conjunction with legal and security concerns
* Help create the sense of psychological safety in the department

## Performance Indicators

* [Infrastructure Cost per GitLab.com Monthly Active Users](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-per-gitlab-com-monthly-active-users)
* [Infrastructure cost vs plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-vs-plan)


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to fill out a short questionnaire.
* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Manager, Data & Analytics
* Next, candidates will be invited to schedule a second interview with a Data Analyst
* Next, candidates will be invited to schedule a third interview with additional members of the Data Team
* Next, if applying for a specialty, candidates will be invited to schedule a fourth interview with our the specialty lead
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
