- name: Candidates Sourced by Recruiting Department vs. Candidates Hired
  base_path: "/handbook/hiring/performance_indicators/"
  definition: How many candidates were sourced by the Recruiting Department out of
    those who we hired in a given month.
  target: The candidates sourced versus candidates hired target is greater than 0.50
  public: true
  org: Recruiting
  is_key: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 8695314
    dashboard: 668158
    embed: v2
    border: false
- name: Team member referrals
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The percentage of Offer Accepts whose source was “Referral” in a given
    calendar month. This is calculated by taking the number of “Referral” Offer Accepts
    and dividing that by the total number of Offer Accepts for a given calendar month.
    “Referrals” are tracked in Greenhouse.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Top of Funnel Diversity
  base_path: "/handbook/hiring/performance_indicators/"
  definition: Percentage of candidates at the sourcing stage of the hiring process
    (aka the "top of the funnel") who would increase diversity at GitLab. GitLab focuses
    on the top of funnel to diversify the initial candidate pool and expand the number
    of qualified candidates in an effort to prevent potential tension between our
    externally communicated diversity hiring goals and our policy of always selecting
    the most qualified candidate. If we set ambitious goals for the top of funnel
    the bottom of funnel goals should be easily achieved.
  target: Outreach to diverse candidates meets or exceeds 90% of all recruitment searches.
  org: Recruiting
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Talent Ops Average Time to Project Completion
  base_path: "/handbook/hiring/performance_indicators/"
  definition: On average, the time it takes to complete one Recruiting Team project.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Talent Ops Project Completion Rate
  base_path: "/handbook/hiring/performance_indicators/"
  definition: How many Recruiting Team projects have been completed versus projects
    that are currently open in a given period of time (e.g. per quarter)
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
  sisense_data:
    chart: 6858936
    dashboard: 482006
    embed: v2
    border: false
- name: Talent Ops Service Desk Vendor Support Tickets
  base_path: "/handbook/hiring/performance_indicators/"
  definition: Analysis of the number of submitted internal support tickets, the number
    of resolved tickets, and the average time to resolution.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Sourced Offers
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of Offers sourced by a Sourcer.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Sourced candidates moved to the Reference Check
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of sourced candidates moved to the Reference Check stage.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Sourcer Hires Conversion Rate
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of candidates submitted by Sourcer that were Hired.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Sourcer Offer Conversion rate
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of candidates submitted by Sourcer that received an Offer.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Sourcer Interview Conversion rate
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of candidates submitted by Sourcer that successfully passed
    the screening call.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Phone Screens Scheduled by a Sourcer
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of candidates scheduled to speak with a Recruiter on a screening
    call.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Sourced Prospects Submitted
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of Prospects submitted to a Recruiter and/or a Hiring Manager
    by one Sourcer.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Social Referrals
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The percentage of Offer Accepts whose source was “SocialReferral” in
    a given calendar month. These candidates applied via links shared on social media
    sites (e.g. LinkedIn, Twitter, and the like). This is calculated by taking the
    number of “SocialReferral” Offer Accepts and dividing that by the total number
    of Offer Accepts for a given calendar month. “SocialReferrals” are tracked in
    Greenhouse.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Recruiting or Hiring Manager LinkedIn Seat
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The percentage of the company that has a recruiting or hiring manager
    seat on LinkedIn. This is calculated by taking the total number of recruiting
    and hiring manager seats divided by total employees on a calendar month basis.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Candidate Time per Stage
  base_path: "/handbook/hiring/performance_indicators/"
  definition: Average number of days a Candidate spends per stages in the [interview
    plan](/handbook/hiring/recruiting-framework/coordinator/#step-14c-schedule-team-interviews).
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
  urls: 
- name: Outbound Candidates versus Candidates Hired
  base_path: "/handbook/hiring/performance_indicators/"
  definition: Outbound candidates are candidates sourced from GitLab Talent Community,
    along with Referrals, Social Referrals, and Candidates Sourced by Recruiting Department.
  target: The goal is that outbound candidates are greater than 80 percent of all
    hired candidates.
  org: Recruiting
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 8695326
    dashboard: 668158
    embed: v2
- name: Active Users with a Recruiting or Hiring Manager LinkedIn Seat
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The percentage of team members that have a *Recruiter* or *Hiring Manager*
    seat and at least 1 active day of use per month. This is calculated by taking
    the total number of team members with at least 1 active day of use and dividing
    it by the total number of team members that have a seat.
  target: 
  org: Recruiting
  is_key: false
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Hires vs. Plan
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The number of hires based on start date divided by the number of planned
    hires in a given month. Hires comes from BambooHR. This KPI is tracked and reported
    on a monthly basis but year to date (headcount v plan) is also measured.
  target: greater than 0.9
  org: Recruiting
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Goal not met
  sisense_data:
    chart: 6888845
    dashboard: 482006
    embed: v2
    border: false
- name: Time to Offer Accept (Days)
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The median number of days from when a candidate is active in Greenhouse
    (applied, referred, or sourced) to accepting an offer in a given month.
  target: The Time to Offer Accept (Days) target is < 45.
  org: Recruiting
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - Time to offer is on target
  sisense_data:
    chart: 6235296
    dashboard: 482006
    embed: v2
    border: false
- name: Interviewee Satisfaction (ISAT)
  base_path: "/handbook/hiring/performance_indicators/"
  definition: The answer to the ‘Interviewee Satisfaction (ISAT) Score’ question,
    “Overall, my interviewing experience was a positive one,” is greater than 4.1
    across all departments in a given month (1 - 5 response scale).
  target: Greater than 4.1
  org: Recruiting
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - ISAT score is under target
  sisense_data:
    chart: 6858936
    dashboard: 482006
    embed: v2
    border: false
- name: Cost Per Hire
  base_path: "/handbook/hiring/performance_indicators/"
  definition: Cost per hire is calculated by taking the total monthly expenses incurred
    in the Recruiting department and other monthly expenses related to the hiring
    process and dividing by the total number of hires in that month. Besides the expenses
    incurred in the Recruiting department (i.e. salaries, travel expenses, recruiting
    software etc.) other expenses related to recruiting are agency fees and referral
    bonuses. These expenses are incurred in departments outside of Recruiting but
    hit their own GL accounts. These GL accounts will be what is included in Cost
    Per Hire, along with the total Recruiting Department expenses. This is calculated
    on a rolling three month average.
  target: The cap is less than $6,500.
  org: Recruiting
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - Cost per hire rising with less hires being made due to change in plan
  urls:
  - https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6946649&udv=841419

  - name: Sourcing for individuals from underrepresented groups
  base_path: /handbook/hiring/performance_indicators/
  definition: Measuring percentage of outbound sourcing efforts that are directed towards individuals from underrepresented groups.
  target: Greater than or equal to 0.95
  public: true
  org: Recruiting
  is_key: true
  health:
    level: 0
    reasons:
      - tbd
  sisense_data:
    chart: 9417379
    dashboard: 711351
    embed: v2
    border: off
